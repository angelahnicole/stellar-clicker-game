# Stellar-Clicker

## WEBSITE

![Untitled-1.jpg](https://bitbucket.org/repo/5aaGnM/images/1566843170-Untitled-1.jpg)

* [SC Website](http://stellar.polymorphixgaming.com)
* [SC Website Source Code](https://bitbucket.org/angelahnicole/stellar-clicker-website)

## DEVELOPMENT ENVIRONMENT
The game has been built with PC in mind. However, since we are using jMonkeyEngine (JME) it supports building the application to Android, Mac, and Linux. Therefore, the game is technically be cross-platform, but we have focused on making the application playable on PC.

In order to gain use of some Java 8 features, we decided on using the Java Runtime Environment of v1.8. JME 3.0 has been used as the game engine for development. It is the most recent stable version released. Together, JDK 1.8 and JME 3.0 will be stable, and has created a better development environment less-buggy game.

## SPRITE ART

Given that we are all primarily programmers and not artists, we have enlisted the help of Andrea Johnson to create our sprite art. This doesn’t contain any animations and will be pixel representations of the ship, its components, its senior staff, and other smaller icons (e.g. clatinum, ship statistics, officers, etc.)

## OTHER ASSETS

For all the other assets, (sound effects, UI components, looping music, etc.), we heavily relied on open source and free materials online:

* Vector Icons: Freepik from www.flaticon.com
* Music: Cycles from Audionautix
* UI Components: Kenney from http://kenney.nl/assets 

Most of these assets were edited with photo editing and audio editing software.

## GAME SCREENS

![Untitled-1.jpg](https://bitbucket.org/repo/5aaGnM/images/4291692133-Untitled-1.jpg)

![Untitled-2.jpg](https://bitbucket.org/repo/5aaGnM/images/3576557381-Untitled-2.jpg)

![Untitled-3.jpg](https://bitbucket.org/repo/5aaGnM/images/2733080922-Untitled-3.jpg)

![Untitled-4.jpg](https://bitbucket.org/repo/5aaGnM/images/1035114278-Untitled-4.jpg)

![Untitled-5.jpg](https://bitbucket.org/repo/5aaGnM/images/2682096607-Untitled-5.jpg)

![Untitled-6.jpg](https://bitbucket.org/repo/5aaGnM/images/3756140719-Untitled-6.jpg)

![Untitled-7.jpg](https://bitbucket.org/repo/5aaGnM/images/51797340-Untitled-7.jpg)

![Untitled-8.jpg](https://bitbucket.org/repo/5aaGnM/images/3992149391-Untitled-8.jpg)

![Untitled-9.jpg](https://bitbucket.org/repo/5aaGnM/images/3265496426-Untitled-9.jpg)